import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appMyFirst]'
})
export class MyFirstDirective implements OnInit{

  @Input() appMyFirst:string;

  constructor(private element: ElementRef) { }

  @HostListener('mouseenter')
  publiconMouseEnter(){

   // this.element.nativeElement.style.backgroundColor = "red";
    this.element.nativeElement.style.backgroundColor = this.appMyFirst;
  }


  @HostListener('mouseleave')
  publiconMouseLeave(){
    this.element.nativeElement.style.backgroundColor = "";

  }

  ngOnInit(){}

}
