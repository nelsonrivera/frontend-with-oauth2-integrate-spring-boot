import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {IUser} from '../../user.interface';
import {UserService} from '../../user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-app-list',
  templateUrl: './app-list.component.html',
  styleUrls: ['./app-list.component.css']
})
export class AppListComponent implements OnInit {
  users$: Observable<IUser[]>;

  constructor(public userService: UserService, private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.init();
  }


  init() {

    this.users$ = this.userService.getUsers();
    this.userService.getUsersFromResource();
  }

  createUser(user) {
    this.userService.createNewUser(user);
  }

}
