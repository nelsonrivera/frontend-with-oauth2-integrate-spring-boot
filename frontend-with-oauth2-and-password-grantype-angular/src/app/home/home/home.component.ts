import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap} from 'rxjs/operators';
import {UserService} from '../../user.service';
import {Cookie} from 'ng2-cookies';
import {IUser} from "../../user.interface";
import jwt_decode from "jwt-decode";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CreateFormModalComponent} from "../../modals/create-form-modal/create-form-modal.component";
import {Router} from "@angular/router";
import { environment } from "../../../environments/environment";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userName: string = '';
  user: IUser;
  accessToken: string;
  adminRol: boolean = false;

  constructor(private http: HttpClient, private userService: UserService, private modalService: NgbModal, private router: Router) { }

  ngOnInit() {
    let url =  `${environment.apiUrl}/users/login`;
    console.log("access token: " + Cookie.get('access_token'))
    this.accessToken = Cookie.get('access_token');

    if(this.accessToken) {
      console.log("demtro", this.accessToken)
      const authorities: string[] = jwt_decode(this.accessToken)['authorities'];
      this.adminRol = authorities.some(rol => rol === 'ROLE_ADMIN');
    }

    let headers: HttpHeaders = new HttpHeaders({
      'Authorization': 'Bearer ' + this.accessToken
    });

    let options = { headers: headers };
    this.http.get<Observable<Object>>(url, options).pipe(map(res => JSON.stringify(res)))
        .subscribe(userResponse => {
        this.user = JSON.parse(userResponse);
      },
      error => {
        console.log('error', error);
        this.user = {};
      }
    );
  }

  registerFreeUser(){
    const modalRef = this.modalService.open(CreateFormModalComponent);

    modalRef.result.then((result) => {
      this.userService.registerFreeUser(result)
          .subscribe(
              data => {
                Cookie.set('access_token', data.token, 30);
                this.accessToken = data.token;
                window.location.reload();
              },
              err => console.log("registering error")
          )

    }).catch((error) => {
      console.log(error);
    });
  }

  logout() {
    this.userService.logout();
  }
}
