import {Observable} from 'rxjs';
import {UserService} from './user.service';
import {Component, ChangeDetectionStrategy} from '@angular/core';
import {IUser} from './user.interface';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  constructor(public userService: UserService, private route: ActivatedRoute) {
  }
}
