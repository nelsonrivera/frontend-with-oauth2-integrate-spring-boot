export interface IUser {
  uuid?: number;
  name?: string;
  email?: string;
  created?: string;
  last_login?: string;
  phones?: any[];
}
