import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import {UserService} from './user.service';
import { HttpClientModule } from '@angular/common/http';
import {MyFirstDirective} from './my-first.directive';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CreateFormModalComponent } from './modals/create-form-modal/create-form-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login/login.component';
import { HomeComponent } from './home/home/home.component';
import { AppListComponent } from './app-list/app-list/app-list.component';


@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    MyFirstDirective,
    CreateFormModalComponent,
    LoginComponent,
    HomeComponent,
    AppListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent],
  entryComponents: [CreateFormModalComponent]
})
export class AppModule { }
