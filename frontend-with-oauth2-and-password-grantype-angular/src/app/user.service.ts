import {EventEmitter, Injectable, Output} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {IUser} from './user.interface';
import {map} from 'rxjs/operators';
import { Cookie } from 'ng2-cookies';
import {Router} from '@angular/router';
import { environment } from "../environments/environment";


@Injectable()
export class UserService {

  private usersSubject = new BehaviorSubject([]);

  private urlApi: string;
  public user$: Observable<IUser[]>;
  @Output() onModifyUserObservable: EventEmitter<any> = new EventEmitter();



  constructor(private httpClient: HttpClient, private router: Router) {
    this.urlApi = environment.apiUrl;
  }


  getUsers(): Observable<IUser[]> {
    return this.usersSubject.asObservable();
  }

  private emitNewUsersList(users: IUser[]) : void{
    this.usersSubject.next(users);
  }

  obtainAccessToken(loginData){
    let params = new URLSearchParams();
    params.append('username',loginData.username);
    params.append('password',loginData.password);
    params.append('grant_type','password');
    params.append('client_id','transBankClientId');
    let headers = new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic '+btoa("transBankClientId:secret")});
    const options = { headers: headers };

    const url = `${this.urlApi}/oauth/token`;

    this.httpClient.post(url, params.toString(), options)
      .pipe(map(res => JSON.stringify(res)))
      .subscribe(
        data => this.saveToken(data),
        err => alert('Invalid Credentials'));
  }

  saveToken(token: any){
    const tokenParse = JSON.parse(token);
    const expireDate = new Date().getTime() + (1000 * tokenParse.expires_in);
    Cookie.set('access_token', tokenParse['access_token'], expireDate);
    this.router.navigate(['/']);
  }

  checkCredentials(){
    if (!Cookie.check('access_token')){
      this.router.navigate(['/login']);
    }
  }

  logout() {
    Cookie.delete('access_token');
    this.router.navigate(['/login']);
  }



  getUsersFromResource(): void{

    const url = `${this.urlApi}/users`;

    let headers: HttpHeaders = new HttpHeaders({
      'Authorization': 'Bearer ' + Cookie.get('access_token')
    });

    let options = { headers: headers };

     this.httpClient.get<IUser[]>(url,options).subscribe(
         respose => {
           this.emitNewUsersList(respose);
         },
         error => {
           console.error(error);
         })
  }


  createNewUser(user: IUser): void {
    const url = `${this.urlApi}/users`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Cookie.get('access_token')
      })
    };


    user.phones = [];
    user.phones[0] = {"number": "456745", "cityCode": "01", "countryCode": "01"};

    const postRequestObservable = this.httpClient
        .post(url, JSON.stringify(user), httpOptions).subscribe(
            user => {
              this.getUsersFromResource();
            },
            error => {
              console.error(error);
            });
  }

  registerFreeUser(user: IUser): Observable<any> {
    const url = `${this.urlApi}/users`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };


    user.phones = [];
    user.phones[0] = {"number": "456745", "cityCode": "01", "countryCode": "01"};

    return this.httpClient
        .post(url, JSON.stringify(user), httpOptions);
  }

}

