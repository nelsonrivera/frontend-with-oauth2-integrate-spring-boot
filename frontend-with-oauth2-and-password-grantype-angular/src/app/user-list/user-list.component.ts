import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, OnChanges } from '@angular/core';
import {IUser} from '../user.interface';
import {CreateFormModalComponent} from '../modals/create-form-modal/create-form-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserListComponent implements OnInit {
  @Input() users;
  @Output() onCreateUser: EventEmitter<any> = new EventEmitter();
  @Output() onDeleteSelected: EventEmitter<any> = new EventEmitter();
  @Output() onModifySelected: EventEmitter<any> = new EventEmitter();
  selectedUser: IUser;


  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  ngOnChanges(){}

  createUser(event:any) {

    const modalRef = this.modalService.open(CreateFormModalComponent);

    modalRef.result.then((result) => {
      this.onCreateUser.emit(result);
      console.log(result)
    }).catch((error) => {
      console.log(error);
    });

  }

  selectUser(event, user: IUser) {
    this.selectedUser = user;
    this.unSelectedAllRows(event.target.parentElement.parentElement);
    console.log(JSON.stringify(this.selectedUser));
    event.target.parentElement.style.backgroundColor = 'red';

  }

  unSelectedAllRows(target){
    for (let row of target.children) {
      row.style.backgroundColor = '';
    }
  }

  openFormModal() {
  }
}
