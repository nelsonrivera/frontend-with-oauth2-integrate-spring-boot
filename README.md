# README #


### ¿Qué contiene este repositorio? ###

* Este repositorio contiene una simple UI construida en Angular y desplegada usando Spring Boot para el manejo de usuarios utilizando un servicio de backend.

### Uso y Configuración ###


   
* Instrucciones para el despliegue
  
      * Construya la aplicación usando: mvn clean install
   
   La aplicación está construida usando Sprint Boot, use el jar generado en la carpeta java -jar angular-spring-boot-application/target/ para levantar la aplicación:
   
   java -jar angular-spring-boot-application/target/angular-spring-boot-application-1.0.jar

* Google CLoud Platform

 La aplicación está disponible además en GCP desplegada en un cluster Kubernetes, puede acceder con la URL:
 
 http://35.198.61.203/index.html

### Contacto ###

* nelsonrivera12@gmail.com
